import datetime
import shutil
from bark import generate_audio, SAMPLE_RATE
import numpy
import scipy.io.wavfile


text_file = "bark-text.txt"
lang_file = "bark-lang.txt"

with open(text_file, "r", encoding="utf-8") as f:
    text_prompts = f.readlines()
text_prompts = [x for x in text_prompts if x.strip()]
text_line = len(text_prompts)

with open(lang_file, "r", encoding="utf-8") as f:
    history_prompt = f.read().strip()
if not isinstance(history_prompt, str):
    history_prompt = "en_speaker_1"
print("\history prompt:", history_prompt)

filename = "bark-" + datetime.datetime.today().strftime("%y%m%d%H%M%S")
audio_file = filename + ".wav"
shutil.copyfile(text_file, filename + ".txt")

audio_arrays = []
for i, text_prompt in enumerate(text_prompts):
    print(f"\n({i+1}/{text_line})Generating {text_prompt} ")
    audio_array = generate_audio(text_prompt, history_prompt=history_prompt)
    audio_arrays.append(audio_array)

print("\nConcatenating...")
combined_audio = numpy.concatenate(audio_arrays)


print("\nWriting to", audio_file)
scipy.io.wavfile.write(
    audio_file,
    SAMPLE_RATE,
    combined_audio,
)
